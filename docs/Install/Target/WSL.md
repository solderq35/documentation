# Windows Subsystem for Linux
This installation method is fundamentally different than others as WSL is a tool to run a Linux terminal within Windows. This is good for how to use the command line, but does not feature a full Linux Desktop.

NOTE: Some software will not work as intended within WSL compared to a normal Linux distribution.

<https://docs.microsoft.com/en-us/windows/wsl/about>

<https://docs.microsoft.com/en-us/windows/wsl/install-win10>