# Launching the Installer

Much like the BIOS section, each installer varies slightly, including a different order. I will include a list of the general steps, although each installer may do them in a different order.

1.  Boot into the Installation USB by restarting your computer with the USB plugged in. 

2.  Start the installation process, using the following as a selection guide. 

### Language and keyboard

In my experience this flawlessly picks the correct default. If it doesn't, manually select the option you want.

### Networking

If possible, connect to a network, although it is not explicitly required for most installers. If prompted, download updates while installing.

### Partitioning

-   If you are dual-booting select "Install Alongside \..." if the
    option exists. If not, see [Linux partitioning](notes.md#linux-partitioning)

-   If you want to wipe the entire drive and just install Linux select
    "Erase disk and install\..."

-   If you need a more advanced configuration, see [Linux partitioning](notes.md#linux-partitioning)

### User

Create your user.

### Finishing Up

Confirm that you choose all of the options that you want. Especially
confirm that you set the partitions up correctly.

That's it! Congrats! For some basic post-install recommendations, see [here](./post-install.md)
