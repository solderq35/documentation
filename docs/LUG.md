# Connect With Us!

## Meetings

Weekly on Tuesdays at 6pm on Jitsi ([link](https://meet.jit.si/WeeklyLUG)) Join one of our social platforms for the
meeting password.

## Mailing List

Join our mailing list [here](http://lists.oregonstate.edu/mailman/listinfo/linux)

## Communication

Matrix is our primary communication method, however Discord is [bridged](https://matrix.org/bridges) for the same
experience on your platform of choice. Bridging to IRC is coming soon!

### Matrix

- Go to [app.element.io](https://app.element.io)
- Create an account if you do not already have an account on a Matrix server
- Join our [community](https://matrix.to/#/#matrix:lug.oregonstate.edu)
- Join our channels at `#general:lug.oregonstate.edu` and `#announcements:lug.oregonstate.edu`

### Discord

- [Install](https://discord.com/download) Discord (or run [in the browser](https://discord.com))
- Join a server by hitting the plus in the lower left hand corner
- Join our server with the invite link: `rwNeYB2`

### IRC

- Open an IRC Client (ex. [Libera Chat's in-browser](https://web.libera.chat/))
- Put in a unique nickname and go to the channel `osu-lug`
- Register your nickname by following the instructions [here](https://libera.chat/guides/registration)

More comprehensive documentation found [here](https://lug.oregonstate.edu/blog/irc/)
