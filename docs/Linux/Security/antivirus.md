# Anti-Virus

ClamAV is a Open Source virus scanner. It does not scan automatically, nor update automatically. The [Arch Wiki](https://wiki.archlinux.org/index.php/ClamAV) shows how to update virus definitions as well as scan.

## Frontends
Clam-gtk is a simple frontend that gives you a one click way to update, or scan a directory. NOTE: This does not search folders recursively by default.
